Rails.application.routes.draw do
	get "/search", to: "articles#search"
	resources :articles do
		# get "/translate", to: "articles#translate"
		resources :article_answers
	end
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

	root to: "articles#index"
end
