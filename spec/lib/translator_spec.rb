require 'rails_helper'

RSpec.describe Translator, type: :lib, vcr: {:match_requests_on => [:method, :uri, :body], record: :once} do
	subject { Translator }

	# before (:all) do
		# VCR.turn_off!
		# stub_request(:post, "https://api.cognitive.microsoft.com/sts/v1.0/issueToken?Subscription-Key=4df88a918dac4c76a6ca03ebd5cadeb8").
        #  with(
        #    headers: {
       	#   'Accept'=>'*/*',
       	#   'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
       	#   'Host'=>'api.cognitive.microsoft.com',
       	#   'User-Agent'=>'Ruby'
        #    }).
        #  to_return(status: 200, body: "", headers: {})
	# end

	let(:text_corpus) { "Hello, World" }
	context "singleton tests" do
		let(:instance1) { subject.instance }
		let(:instance2) { subject.instance }
		it "returns identical instances" do
			expect(instance1).to equal(instance2)
		end
	end
	context "#translate" do
		# TODO: Fix test
		it "succeeds" do
			expect(subject.instance.translate(text_corpus)).to eq("Hallo Welt")
		end
		# VCR.use_cassette("translate_request", :match_requests_on => [:method, :uri, :payload], record: :once) do
		# 	it "succeeds" do
		# 		expect(subject.instance.translate(text_corpus)).to eq("Hallo Welt")
		# 	end
		# end
	end
end
