FactoryBot.define do
	factory :article do
		question { FFaker::Lorem.phrase }
		tag { FFaker::Tweet.tags }

		association :article_answer, factory: :article_answer, strategy: :build
		factory :sensical_article do
			question { FFaker::CheesyLingo.sentence}
			association :article_answer, factory: :sensical_answer, strategy: :build
		end
	end
	factory :article_answer do
		answer { FFaker::Lorem.paragraph }
		factory :sensical_answer do
			answer { FFaker::CheesyLingo.paragraph }
		end
	end
end
