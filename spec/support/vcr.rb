
VCR.configure do |config|
	config.cassette_library_dir = "#{::Rails.root}/spec/cassettes"
	config.hook_into :webmock
	config.ignore_localhost = true
	config.configure_rspec_metadata!
	# config.debug_logger = $stderr
	# config.allow_http_connections_when_no_cassette = true
  end
