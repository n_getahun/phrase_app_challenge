require 'rails_helper'

RSpec.describe Article, type: :model, vcr: {:match_requests_on => [:method, :uri], record: :once} do
	subject { build(:article) }
	context 'associations' do
		it { should have_one(:article_answer).class_name('ArticleAnswer') }
	end
end
