require 'rails_helper'

RSpec.describe ArticleAnswer, type: :model, vcr: {:match_requests_on => [:method, :uri], record: :once} do
	subject do
		@article = create(:article)
		@article.article_answer
	end

	describe "model validation" do
		it { should validate_presence_of(:answer) }
	end

	describe "Association" do
		it { should belong_to(:article) }
	end
end
