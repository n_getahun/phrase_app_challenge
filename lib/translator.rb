require 'uri'
require 'net/https'
require 'json'

# Singleton class to handle authentication and translation of text
class Translator
	@instance = new
	@auth_token = nil
	@authentication_request_time = nil

	private_class_method :new

	class << self
		def instance
			@instance
		end
	end

	def auth_token
		@auth_token
	end

	# code excerpt taken from https://github.com/MicrosoftTranslator/Text-Translation-API-V3-Ruby/blob/master/Translate.rb
	def translate(text, to_lang='de')
		api_key = ENV.fetch("MSFT_TRANSLATOR_APIKEY")
		translate_endpoint = ENV.fetch("MSFT_TRANSLATOR_ENDPOINT")

		path = "/translate?api-version=3.0"
		params = "&to=#{to_lang}"
		uri = URI(translate_endpoint + path + "&to=" + to_lang)
		payload = '[{"Text" : "' + text + '"}]'

		# pre-flight checks
		unless has_valid_token? && @auth_token
			authenticate
		end
		request = Net::HTTP::Post.new(uri)
		request['Content-type'] = 'application/json; charset=UTF-8'
		request['Content-length'] = payload.length
		request['Authorization'] = 'Bearer ' + @auth_token
		request['X-ClientTraceId'] = SecureRandom.uuid
		request.body = payload
		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
			http.request (request)
		end

		result = response.body.force_encoding("utf-8")

		json = JSON.parse(result)
		# puts json, text
		unless json.is_a? Array
			# temporary work around
			" "
		else
			json[0]['translations'][0]['text']
		end
	end

	private
	# Fetch authentication keys
	def authenticate
		auth_endpoint = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken"
		subscription_key = ENV.fetch("MSFT_TRANSLATOR_APIKEY")
		params = "?Subscription-Key=" + subscription_key

		uri = URI(auth_endpoint+params)
		payload = ""

		http_request = Net::HTTP::Post.new(uri)
		http_request.body = payload

		@authentication_request_time = Time.zone.now
		response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
			http.request(http_request)
		end
		# the response is a jwt access key
		@auth_token = response.body
	end

	private
	def has_valid_token?
		# Authentication token needs to be refreshed every 10 mins, the
		# low threshold is 8 mins in our case
		auth_request_time = @authentication_request_time || Time.zone.now
		time_elapsed_in_minutes = (Time.zone.now - auth_request_time).to_int / 60
		time_elapsed_in_minutes < 8 ? true : false
	end
end
