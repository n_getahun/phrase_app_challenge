class ArticleAnswersController < ApplicationController
  before_action :set_article, only: [:new, :edit, :create, :update, :destroy]

  # GET /article_answers
  # GET /article_answers.json
  def index
    @article_answers = ArticleAnswer.all
  end

  # GET /article_answers/1
  # GET /article_answers/1.json
  def show
  end

  # GET /article_answers/new
  def new
    @article_answer = ArticleAnswer.new
  end

  # GET /article_answers/1/edit
	def edit
		# byebug
		@article_answer = @article.article_answer
  end

  # POST /article_answers
  # POST /article_answers.json
  def create
    @article_answer = @article.create_article_answer!(article_answer_params)
		respond_to do |format|
      if @article_answer.save
				# byebug
        format.html { redirect_to @article, notice: 'Article answer was successfully created.'}
        format.json { render :show, status: :created, location: @article }
      else
				byebug
        format.html { render :new }
        format.json { render json: @article_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /article_answers/1
  # PATCH/PUT /article_answers/1.json
	def update
		# byebug
		@article_answer = @article.article_answer
    respond_to do |format|
      if @article_answer.update(update_article_answer_params)
        format.html { redirect_to @article, notice: 'Article answer was successfully updated.' }
        format.json { render :show, status: :ok, location: @article_answer }
      else
        format.html { render :edit }
        format.json { render json: @article_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /article_answers/1
  # DELETE /article_answers/1.json
	def destroy
		# byebug
    @article_answer = ArticleAnswer.find(delete_article_answer_params["id"])
    respond_to do |format|
      format.html { redirect_to @article, notice: 'Article answer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
		def set_article
			@article = Article.find(params[:article_id])
		end
		# def set_article_answer
    #   @article_answer = ArticleAnswer.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_answer_params
      # params.require(:article_answer).permit(:answer, :article_id, :id)
			params.require(:article_answer).permit(:answer)
		end

		def update_article_answer_params
			params.require(:article_answer).permit(:answer)
		end

		def delete_article_answer_params
			params.permit(:id)
		end
end
