class Article < ApplicationRecord
	validates :question, presence: true

	has_one :article_answer

	include PgSearch::Model

	before_save do
		self.question = Translator.instance.translate(self.question)
	end

	pg_search_scope(
		:search,
		against:{
			question: 'A',
			tag: 'B'
		},
		using: {
			tsearch: {
				tsvector_column: "tsv",
				dictionary: "english",
				highlight: {
					StartSel: '<b>',
					StopSel: '</b>',
					MaxWords: 123,
					MinWords: 456,
					ShortWord: 4,
					HighlightAll: true,
					MaxFragments: 3,
					FragmentDelimiter: '&hellip;'
				}
			}
		}
	)
end
