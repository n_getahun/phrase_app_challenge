class ArticleAnswer < ApplicationRecord
	validates :answer, presence: true

	belongs_to :article

	before_create do
		# perform machine translation
		self.translation = Translator.instance.translate(self.answer)
	end

end
