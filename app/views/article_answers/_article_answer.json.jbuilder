json.extract! article_answer, :id, :created_at, :updated_at
json.url article_answer_url(article_answer, format: :json)
