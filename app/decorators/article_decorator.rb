class ArticleDecorator < Draper::Decorator
	delegate_all
	decorates_finders
	def answer
		if object.article_answer.nil?
			" "
		else
			object.article_answer.answer
		end
	end
	def translation
		if object.article_answer.nil?
			" "
		else
			object.article_answer.translation.nil? ? " " : object.article_answer.translation
		end
	end

	def excerpt
		self.translation[0...100]
	end
	def has_answer?
		object.article_answer.nil? == false
	end
end
