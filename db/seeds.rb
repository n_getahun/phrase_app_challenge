# URL: https://www.namecheap.com/support/knowledgebase/

require 'open-uri'
require 'nokogiri'
require 'json'


url = "https://www.namecheap.com/support/knowledgebase/"
main_url = "https://www.namecheap.com/"
html = open(url)
doc = Nokogiri::HTML(html)

section_html = doc.at("div.services.kb-services.five.group").css("ul li")

def truncate_string(text)
	text.length > 250 ? "#{text[0...250]}..." : text
end

def fetch_article_question_answers(parent_url, article_answer_link)
	article_text_html = open(parent_url + article_answer_link)
	article_html = Nokogiri::HTML(article_text_html)
	# byebug
	article_text = article_html
	.css("#ctl00_ctl00_ctl00_ctl00_base_content_web_base_content_home_content_page_content_left_ArticleInfoDisplayControl1 span")
	.css(":not(h1)")
	.css(":not([align='center'])")

	return truncate_string(article_text.collect(&:text).join(" "))
end

def fetch_articles_from_link(parent_url, link)
	fetch_link = parent_url+ link
	html = open(parent_url + link)
	doc = Nokogiri::HTML(html)
	article = doc.css("ul.article-list").css("li")

	result = []
	# Fetch 3 Questions
	article.take(3).each do |aq|
		v = aq.css("p a")
		article_question = v.text
		article_link = v.attr("href").text
		article_link[0] = ''
		article_text = fetch_article_question_answers(parent_url, article_link)
		result << [article_question, article_text]
	end

	return result
end



section_html.each do |el|
	link = el.at("a")['href']
	link[0] = ''
	tag = el.at("a h2").text
	result = fetch_articles_from_link(main_url, link)
	result.each do |e|
		art = Article.create(question: e[0], tag: tag)
		art.create_article_answer!(answer: e[1])
	end
end

