class AddTranslationCaching < ActiveRecord::Migration[5.2]
	def up
		remove_column :article_answers, :markup
		add_column :article_answers, :translation, :text
	end

	def down
		add_column :article_answers, :markup
		remove_column :article_answers, :translation
	end
end
