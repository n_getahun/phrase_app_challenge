class AddArticleAnswerMarkup < ActiveRecord::Migration[5.2]
	def up
		add_column :article_answers, :markup, :text
	end

	def down
		remove_column :article_answers, :markup
	end
end
