class CreateArticleAnswers < ActiveRecord::Migration[5.2]
  def change
		create_table :article_answers do |t|
			# t.bigint :article_id
			t.text :answer
			t.belongs_to :article
      t.timestamps
    end
  end
end
