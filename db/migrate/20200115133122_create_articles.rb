class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :question
      t.string :tag

      t.timestamps
    end
		add_index :articles, :question, unique: true
  end
end
