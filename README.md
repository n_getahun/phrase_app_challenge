# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
	- ```2.6.0```

* System dependencies
	- Install app dependencies by running ```bundle install```

* Database creation
Create postgres database with the following commands:
	- ```createuser --interactive```, and specicify the user name as ```phrase_user```

* App setup
	- ```bundle exec rails db:setup```
	- ```bundle exec rails db:migrate```

* Database seeding
	The seeding script scrapes the namecheap knowledge base and populates the database.
	- ```bundle exec rails db:seed```

* Configuration
	- specify the microsoft translate api key in ```.env.development``` file ```MSFT_TRANSLATOR_APIKEY```

* How to run the test suite
	- ```bundle exec rspec```
	- When introducing new test that uses the translation api, make sure to delete the related recording in ```spec/cassettes```

* starting the server
	- ```bundle exec rails s```
